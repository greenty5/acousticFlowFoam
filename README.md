# acousticFlowFoam
by taka

### Change log
- 2019.2.1 
- 2020.12.9 

### Description

Transient solver for incompressible, turbulent flow, using the PISO
algorithm.
Acoustic power calculation code is added in this solver.
The solver is based on pisoFoam (OF v.8).

Sub-models include:
- turbulence modelling, i.e. laminar, __RAS__ or __LES__
- but in order to calculaet acoustic power, __LES__ have to be loadded in the calculation
- run-time selectable MRF and finite volume options, e.g. explicit porosity

- acoustic power is calculated by means of __k__ and __epsilon__. 
- epsilon is estimated based on an isotoropic assumption of wave propagation [^1].
- epsilon = 1.5*pow(U, 3.0)/ (mesh.V()/mesh.magSf());
- $` \epsilon = \frac{1.5 \cdot U^{3.0}}{mesh.V()/mesh.magSf()} `$

### References
[^1]: Geoffrey M. Lilley, et al., The Radiated Noize From Isotropic Turbulence Revisited, ICASE Report No.93-75, 1993.
